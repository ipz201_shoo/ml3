# Імпортуємо необхідні бібліотеки
import numpy as np
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
from sklearn import datasets

# Завантажуємо набір даних Iris
iris = datasets.load_iris()
X = iris.data

# Визначаємо кількість кластерів (три класи Iris)
num_clusters = 3

# Створюємо об'єкт KMeans для кластеризації
kmeans = KMeans(n_clusters=num_clusters, random_state=0, n_init=10)

# Навчання моделі
kmeans.fit(X)

# Передбачення кластерів для кожного прикладу
labels = kmeans.predict(X)

# Відображення кластерів на графіку
plt.scatter(X[:, 0], X[:, 1], c=labels, s=50, cmap='viridis')
plt.xlabel('Sepal length')
plt.ylabel('Sepal width')

# Відображення центрів кластерів
centers = kmeans.cluster_centers_
plt.scatter(centers[:, 0], centers[:, 1], c='black', s=200, alpha=0.5, label='Centers of clusters')
plt.legend()

# Виведення результатів
plt.title('K-means clustering for Iris')
plt.show()
