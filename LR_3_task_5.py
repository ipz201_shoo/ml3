import numpy as np
import matplotlib.pyplot as plt
from sklearn.linear_model import LinearRegression
from sklearn.preprocessing import PolynomialFeatures
from sklearn.metrics import mean_squared_error, r2_score, mean_absolute_error

m = 100
X = 6 * np.random.rand(m, 1) - 5
y = 0.7 * X ** 2 + X + 3 + np.random.randn(m, 1)

lin_reg = LinearRegression()
lin_reg.fit(X, y)

poly_features = PolynomialFeatures(degree=2, include_bias=False)
X_poly = poly_features.fit_transform(X)
poly_reg = LinearRegression()
poly_reg.fit(X_poly, y)

print("Linear regression:")
print("Coefficient: ", lin_reg.coef_)
print("Intercept: ", lin_reg.intercept_)

print("\nPolynomial regression:")
print("Coefficients: ", poly_reg.coef_)
print("Intercept: ", poly_reg.intercept_)

plt.scatter(X, y, label='Data', s=10)
plt.xlabel('X')
plt.ylabel('Y')

y_lin_pred = lin_reg.predict(X)
plt.plot(X, y_lin_pred, color='red', label='Linear regression')

y_poly_pred = poly_reg.predict(X_poly)
plt.plot(X, y_poly_pred, color='green', label='Polynomial regression')

plt.legend()
plt.show()

print("\nQuality score for linear regression:")
print("R2 Score:", r2_score(y, y_lin_pred))
print("Mean Absolute Error:", mean_absolute_error(y, y_lin_pred))
print("Mean Squared Error:", mean_squared_error(y, y_lin_pred))

print("\nQuality score for polynomial regression:")
print("R2 Score:", r2_score(y, y_poly_pred))
print("Mean Absolute Error:", mean_absolute_error(y, y_poly_pred))
print("Mean Squared Error:", mean_squared_error(y, y_poly_pred))
